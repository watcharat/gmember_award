//
//  VRDViewController.m
//  GMEMBER AWARD2
//
//  Created by Tukta on 11/28/2556 BE.
//  Copyright (c) 2556 gmm. All rights reserved.
//

#import "VRDViewController.h"

@interface VRDViewController ()

@end

@implementation VRDViewController
@synthesize tableViewVRD;
@synthesize lblTitle;
@synthesize TitleName;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     self.title=@"ผลการโหวตล่าสุด";
    lblTitle.text = TitleName;
    NSLog(@"lbltie = %@ ",TitleName);
        self.navigationItem.leftBarButtonItem = [self getBackBtn];
}


- (UIBarButtonItem *) getBackBtn
{
    UIButton * backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [backBtn setFrame:CGRectMake(0.20f,0.50f,14.0f,20.0f)];
    [backBtn setImage:[UIImage imageNamed:[NSString stringWithFormat:@"back.png"]] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
    return backBarButton;
}

//Action For LeftBarButton - BackButton
- (void)backBtnPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"VRDCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    //    cell.textLabel.text = [recipes objectAtIndex:indexPath.row];
    UIImageView *imgNameVRD = (UIImageView *)[cell viewWithTag:1];
    imgNameVRD.image = [UIImage imageNamed:@"pic_177x199.jpg"];
    
     UIImageView *imgicon = (UIImageView *)[cell viewWithTag:2];
     imgicon.image = [UIImage imageNamed:@"heart_ss.png"];
    
     UILabel *lblNameVRD = (UILabel *)[cell viewWithTag:3];
    lblNameVRD.text =@"MinHo";
    
    UILabel *lblPerVRD = (UILabel *)[cell viewWithTag:4];
    lblPerVRD.text = @"80%";
    
    return cell;
}






@end
