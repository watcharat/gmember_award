//
//  ViewController.m
//  GMEMBER AWARD2
//
//  Created by Tukta on 11/19/2556 BE.
//  Copyright (c) 2556 gmm. All rights reserved.
//

#import "ViewController.h"
#import "AFNetworking.h"
@interface ViewController ()

@end

@implementation ViewController
@synthesize imgBanner;
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES ];
    
    // Countdown
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateLabel) userInfo:nil repeats:YES];
//    NSLog(@"%@", timer);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:
                             [NSURL URLWithString:@"http://thestar9.gmmwireless.com:80/thestar9/api_awards/index.php/banner/get_ios"]];
//    [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id json) {
        
        result = [json objectForKey:@"DATA"];
        NSString *status =[json objectForKey:@"STATUS_CODE"];
        NSString *test= [result objectAtIndex:0];
        NSLog(@"status %@", status);
        NSLog(@"DATA %@", test);
        
        [imgBanner setImageWithURL:[NSURL URLWithString:test]
                  placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        
        //        [self updateUI];
    } failure:nil];
    
    [operation start];

}

// Time Countdown
//-------------------------------------------------------------------
-(void) updateLabel {
    NSString *myDate1 = @"2013-12-6 18:00:00";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *myDate = [dateFormatter dateFromString:myDate1];
//    NSLog(@"%@", myDate);
    NSTimeInterval oldTime = [myDate timeIntervalSince1970];
    destinationDate = [NSDate dateWithTimeIntervalSince1970:oldTime];
    
    NSCalendar *calender = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    int units = NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *components = [calender components:units fromDate:[NSDate date] toDate:destinationDate options:0];
    
    if (components.second<0) {
        [timer invalidate];
        timer = nil;
        
    }else{
        lblday.text=[NSString stringWithFormat:@"%02ld",(long)(components.day)];
        lblhour.text=[NSString stringWithFormat:@"%02ld",(long)(components.hour)];
        lblminutes.text=[NSString stringWithFormat:@"%02ld",(long)(components.minute)];
        lblseconds.text=[NSString stringWithFormat:@"%02ld",(long)(components.second)];
        
        
    }
    
    UIImage *plate1 = [UIImage imageNamed:@"banner"];
    [imgBanner setImage:plate1];
   
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)gotoVote:(id)sender {
    [self performSegueWithIdentifier:@"line1" sender:sender];
}

- (IBAction)gotoWinner:(id)sender {
    [self performSegueWithIdentifier:@"line2" sender:sender];
}

- (IBAction)gotoHowto:(id)sender {
    [self performSegueWithIdentifier:@"line3" sender:sender];
}
@end
