//
//  VoteViewController.h
//  GMEMBER AWARD2
//
//  Created by Tukta on 11/19/2556 BE.
//  Copyright (c) 2556 gmm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VoteViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>{
    NSTimer *timer;
    NSDate *destinationDate;
    IBOutlet UILabel *lblday1;
    IBOutlet UILabel *lblhour1;
    IBOutlet UILabel *lblminutes1;
    IBOutlet UILabel *lblseconds1;

       
}
- (IBAction)close:(id)sender;
@property (nonatomic, strong) IBOutlet UITableView *tableViewVote;

@end
