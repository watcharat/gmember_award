//
//  AppDelegate.h
//  GMEMBER AWARD2
//
//  Created by Tukta on 11/19/2556 BE.
//  Copyright (c) 2556 gmm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
