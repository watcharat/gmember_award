//
//  WinnerViewController.m
//  GMEMBER AWARD2
//
//  Created by Tukta on 11/22/2556 BE.
//  Copyright (c) 2556 gmm. All rights reserved.
//

#import "WinnerViewController.h"

@interface WinnerViewController ()

@end

@implementation WinnerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backMenu:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
