//
//  ViewController.h
//  GMEMBER AWARD2
//
//  Created by Tukta on 11/19/2556 BE.
//  Copyright (c) 2556 gmm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    
    NSDate *destinationDate;
    NSTimer *timer;
    NSMutableArray *result;
    
    IBOutlet UILabel *lblday;
    IBOutlet UILabel *lblhour;
    IBOutlet UILabel *lblminutes;
    IBOutlet UILabel *lblseconds;
    
}
- (IBAction)gotoVote:(id)sender;
- (IBAction)gotoWinner:(id)sender;
- (IBAction)gotoHowto:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *imgBanner;

@end
