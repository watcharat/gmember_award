//
//  VoteResultViewController.m
//  GMEMBER AWARD2
//
//  Created by Tukta on 11/22/2556 BE.
//  Copyright (c) 2556 gmm. All rights reserved.
//

#import "VoteResultViewController.h"
#import "VRDViewController.h"
#import "VoteDetailViewController.h"
@interface VoteResultViewController ()

@end

@implementation VoteResultViewController
@synthesize tableViewVR;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"ผลการโหวตล่าสุด";
     tt = [NSArray arrayWithObjects:@"Egg Benedict", @"Mushroom Risotto", @"Full Breakfast", @"Hamburger", @"Ham and Egg Sandwich", @"Creme Brelee", @"White Chocolate Donut", @"Starbucks Coffee", @"Vegetable Curry", @"Instant Noodle with Egg", @"Noodle with BBQ Pork", @"Japanese Noodle with Pork", @"Green Tea", @"Thai Shrimp Cake", @"Angry Birds Cake", @"Ham and Cheese Panini", nil];
    self.navigationItem.leftBarButtonItem = [self getBackBtn];
}
    
    
- (UIBarButtonItem *) getBackBtn
    {
        UIButton * backBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        [backBtn setFrame:CGRectMake(0.50f,0.50f,47.0f,26.0f)];
        [backBtn setImage:[UIImage imageNamed:[NSString stringWithFormat:@"cancel.png"]] forState:UIControlStateNormal];
        [backBtn addTarget:self action:@selector(backBtnPressed) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *backBarButton = [[UIBarButtonItem alloc] initWithCustomView:backBtn];
        return backBarButton;
    }
    
    //Action For LeftBarButton - BackButton
- (void)backBtnPressed
    {
        [self.navigationController popViewControllerAnimated:YES];
    }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tt count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"VRListcell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    //    cell.textLabel.text = [recipes objectAtIndex:indexPath.row];
    
    UIImageView *imgicon = (UIImageView *)[cell viewWithTag:1];
    imgicon.image = [UIImage imageNamed:@"artistrock_a.png"];
 
    UILabel *lblNameVote = (UILabel *)[cell viewWithTag:2];
    lblNameVote.text =[tt objectAtIndex:indexPath.row];
    
    //    UILabel *recipeDetailLabel = (UILabel *)[cell viewWithTag:102];
    //    recipeDetailLabel.text = recipe.detail;
    
    return cell;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"SDVResult"]) {
        NSIndexPath *indexPath = [self.tableViewVR indexPathForSelectedRow];
        VRDViewController *destViewController = segue.destinationViewController;
        destViewController.TitleName = [tt objectAtIndex:indexPath.row];
        destViewController.title=@"ผลการโหวตล่าสุด";
        
        
    }
}


@end
