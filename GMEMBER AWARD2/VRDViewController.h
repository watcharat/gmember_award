//
//  VRDViewController.h
//  GMEMBER AWARD2
//
//  Created by Tukta on 11/28/2556 BE.
//  Copyright (c) 2556 gmm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VRDViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>{
    
   
}
@property (nonatomic, strong) IBOutlet UITableView *tableViewVRD;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (nonatomic, strong) NSString *TitleName;
@end
