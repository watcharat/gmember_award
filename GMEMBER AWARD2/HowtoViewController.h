//
//  HowtoViewController.h
//  GMEMBER AWARD2
//
//  Created by Tukta on 11/21/2556 BE.
//  Copyright (c) 2556 gmm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HowtoViewController : UIViewController<UIScrollViewDelegate>{
    
    
    IBOutlet UIScrollView *sv;
    IBOutlet UIPageControl *page;
    IBOutlet UIWebView *wb;
    NSArray *Arr;
    int TimeNum;
    BOOL Tend;

}

- (IBAction)close:(id)sender;

@end
