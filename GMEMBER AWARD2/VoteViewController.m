//
//  VoteViewController.m
//  GMEMBER AWARD2
//
//  Created by Tukta on 11/19/2556 BE.
//  Copyright (c) 2556 gmm. All rights reserved.
//

#import "VoteViewController.h"
#import "VoteResultViewController.h"
@interface VoteViewController ()

@end

@implementation VoteViewController{
    NSArray *list;
  
}

@synthesize tableViewVote;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [self.navigationController setNavigationBarHidden:YES ];
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIColor whiteColor],NSForegroundColorAttributeName,
                                    [UIColor whiteColor],NSBackgroundColorAttributeName,
                                    [UIFont fontWithName:@"HelveticaNeue-CondensedBlack" size:30.0], NSFontAttributeName,nil];
    
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.title = @"V O T E";
    
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateLabel) userInfo:nil repeats:YES];
    NSLog(@"%@", timer);
    
    [super viewDidLoad];
	// Initialize table data
    list = [NSArray arrayWithObjects: @"สุดยอดศิลปินชายแห่งปี ",@"สุดยอดศิลปินหญิงแห่งปี",@"สุดยอดศิลปินกลุ่มแหง่ปี",@" สุดยอดศิลปินร็อคแห่งปี ",@"สุดยอดศิลปินน่าจับตามองแห่งปี ",@"สุดยอดศิลปินลูกทุ่งชายแหง่ปี",@" สุดยอดศิลปินลูกทุ่งหญิงแห่งปี ",@"สุดยอดเพลงฮิตแห่งปี ",@" สุดยอดเพลงร็อคแห่งปี ",@" สุดยอดเพลงลกูทุ่งแห่งปี ",@" สุดยอดเพลงประกอบภาพยนตร์ ",@"สุดยอดเพลงละครแห่งปี ",@" สุดยอด MV แห่งปี ",@" สุดยอดผู้สร้างสีสนัแห่งปี ",@"สุดยอด Best Couple แห่งปี",nil];
  
}

// Time Countdown
//-------------------------------------------------------------------
-(void) updateLabel {
    NSString *myDate1 = @"2013-12-6 18:00:00";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *myDate = [dateFormatter dateFromString:myDate1];
    NSLog(@"%@", myDate);
    NSTimeInterval oldTime = [myDate timeIntervalSince1970];
    destinationDate = [NSDate dateWithTimeIntervalSince1970:oldTime];
    
    NSCalendar *calender = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    int units = NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *components = [calender components:units fromDate:[NSDate date] toDate:destinationDate options:0];
    
    if (components.second<0) {
        [timer invalidate];
        timer = nil;
        
    }else{
        lblday1.text=[NSString stringWithFormat:@"%02ld",(long)(components.day)];
        lblhour1.text=[NSString stringWithFormat:@"%02ld",(long)(components.hour)];
        lblminutes1.text=[NSString stringWithFormat:@"%02ld",(long)(components.minute)];
        lblseconds1.text=[NSString stringWithFormat:@"%02ld",(long)(components.second)];
        
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)close:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [list count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"ListVoteCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
//    cell.textLabel.text = [list objectAtIndex:indexPath.row];
    
    UIImageView *recipeImageView = (UIImageView *)[cell viewWithTag:100];
    recipeImageView.image = [UIImage imageNamed:@"artistrock_a.png"];
    
    UILabel *recipeNameLabel = (UILabel *)[cell viewWithTag:110];
    recipeNameLabel.text =[list objectAtIndex:indexPath.row];
    

    return cell;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showDetailVote"]) {
        NSIndexPath *indexPath = [self.tableViewVote indexPathForSelectedRow];
       
        VoteResultViewController *destViewController = segue.destinationViewController;
        destViewController.title = [list objectAtIndex:indexPath.row];

    }
    else if ([segue.identifier isEqualToString:@"showDetailVote1"]) {
        NSIndexPath *indexPath = [self.tableViewVote indexPathForSelectedRow];
        
        VoteResultViewController *destViewController = segue.destinationViewController;
        destViewController.title = [list objectAtIndex:indexPath.row];
        
    }
}




@end
